program swimo;
uses crt,neo6502,neo6502math;
const 
    SWIM_MAX = 48;
    FOOD_MAX = 100;
    TRAIL_MAX = 1000;

    NONE = $ff;
    MOVE_SPEED = 0.03;
    FRICTION = 1.07;
    ENERGY_MAX = 5000;
    START_ENERGY = 5000 div 5;
    T_HUNGRY = START_ENERGY;
    T_MATING = ENERGY_MAX - 1000;
    HUNGRY_HASTE = 5;
    //T_MATING = MAX_ENERGY * 0.75;

    FOOD_ENERGY = 1000;
    FEED_DELAY = 20;

    POOL_TOP = 1;
    POOL_BOTTOM = 238;
    POOL_LEFT = 1;
    POOL_RIGHT = 238;
    POOL_WIDTH = POOL_RIGHT - POOL_LEFT;
    POOL_HEIGHT = POOL_BOTTOM - POOL_TOP;

    COLOR_WATER = 8;
    TRAIL_DELAY = 20;
    COLOR_TRAIL = 9;
    COLOR_FEEDER = 9;

    FEEDER_MODES_COUNT = 10;
    BUT_FEEDER_TOP = 32;
    BUT_NEW = 64;
    BUT_CLEAR = 216;
    
    P_MOVE_FREQ_MIN = 20;
    P_MOVE_FREQ_MAX = 50;

    P_MOVE_FORCE_MIN = 10;
    P_MOVE_FORCE_MAX = 30;

    P_SIGHRANGE_MIN = 40;
    P_SIGHRANGE_MAX = 80;

    P_LIVELEN_MIN = 180;
    P_LIVELEN_MAX = 300;

var 
    s_x,s_y,
    s_force: array [0..SWIM_MAX] of float;

    s_energy: array [0..SWIM_MAX] of SmallInt;
    
    s_dir,
    s_livelength,
    s_age: array [0..SWIM_MAX] of word;
    
    s_foodTarget,
    s_mateTarget,
    s_moveforce,
    s_sighrange,
    s_movet,
    s_t,
    s_sprite,
    s_movefreq: array [0..SWIM_MAX] of byte;

    f_x: array [0..FOOD_MAX] of float;
    f_y: array [0..FOOD_MAX] of float;
    
    t_x: array [0..TRAIL_MAX] of word;
    t_y: array [0..TRAIL_MAX] of byte;
    t_time: array [0..TRAIL_MAX] of word;
    
    joy:byte;
    i:byte;
    k:char;
    ftemp:float;
    count,swimmerCount,foodCount:byte;
    autoFeederCounter:word;
    tmpstr:TString;
    vblc:cardinal;
    frame:word;
    trailsShowPtr,trailsHidePtr:word;
    
    my,mBut,mButPrev,mWheel,mWheelPrev:byte;
    mx:word;
    manualFeedDelay:byte;
    autoFeeder:byte;

    autoFeederNames:array [0..9] of string[4] = ('Off','0.1s','0.2s','0.5s','1s','2s','3s','4s','5s','10s');
    autoFeederDelay:array [0..9] of word = (0,6,12,30,60,120,180,240,300,600);

procedure AddTrail(si:byte);
begin
    if t_y[trailsShowPtr] = NONE then begin
        t_time[trailsShowPtr] := frame + TRAIL_DELAY;
        t_y[trailsShowPtr] := Trunc(s_y[si]);
        t_x[trailsShowPtr] := Trunc(s_x[si]);
        NeoSetColor(COLOR_TRAIL);
        NeoSetColor(COLOR_TRAIL);
        NeoSetColor(COLOR_TRAIL);
        NeoDrawPixel(t_x[trailsShowPtr],t_y[trailsShowPtr]);
        Inc(trailsShowPtr);
        if trailsShowPtr = TRAIL_MAX then trailsShowPtr:=0;
    end;
end;

procedure ShowFeeder;
var fcol:byte;
begin
    if autoFeeder>0 then fcol:=$f else fcol:=$8;
    NeoSetColor(10);
    NeoSetSolidFlag(1);
    NeoDrawRect(244,BUT_FEEDER_TOP,316,BUT_FEEDER_TOP+21);
    NeoSetSolidFlag(0);
    NeoSetColor(fcol);
    tmpstr:='Auto feeder';
    NeoDrawString(247,BUT_FEEDER_TOP+2,tmpstr);
    tmpstr:=autoFeederNames[autoFeeder];
    NeoDrawString(247,BUT_FEEDER_TOP+12,tmpstr);
    NeoSetColor(5);
    tmpstr:='[A]';
    NeoDrawString(296,BUT_FEEDER_TOP+12,tmpstr);
end;

procedure ShowNewBeing;
var fcol:byte;
begin
    NeoSetColor(9);
    NeoSetSolidFlag(1);
    NeoDrawRect(244,BUT_NEW,316,BUT_NEW+21);
    NeoSetColor($f);
    NeoSetSolidFlag(0);
    tmpstr:='Plant being';
    NeoDrawString(247,BUT_NEW+2,tmpstr);
    NeoSetColor(5);
    tmpstr:='[B]';
    NeoDrawString(296,BUT_NEW+12,tmpstr);
end;

procedure ShowClear;
var fcol:byte;
begin
    NeoSetColor(8);
    NeoSetSolidFlag(1);
    NeoDrawRect(244,BUT_CLEAR,316,BUT_CLEAR+21);
    NeoSetColor($f);
    NeoSetSolidFlag(0);
    tmpstr:='Purge pond';
    NeoDrawString(247,BUT_CLEAR+2,tmpstr);
    NeoSetColor(5);
    tmpstr:='[P]';
    NeoDrawString(296,BUT_CLEAR+12,tmpstr);
end;


procedure ToggleFeeder;
begin
    autoFeeder := autoFeeder + 1;
    if autoFeeder = FEEDER_MODES_COUNT then autoFeeder := 0;
    autoFeederCounter := 0;
    ShowFeeder;
end;

procedure ClearTrails;
var ft:word;
begin
     for ft:=0 to TRAIL_MAX do t_y[ft] := NONE;
end;

procedure ExpireTrails;
begin
    while (t_y[trailsHidePtr] <> NONE) and (t_time[trailsHidePtr] = frame) do begin
        NeoSetColor(COLOR_WATER);
        NeoDrawPixel(t_x[trailsHidePtr],t_y[trailsHidePtr]);
        t_y[trailsHidePtr] := NONE;
        Inc(trailsHidePtr);
        if trailsHidePtr = TRAIL_MAX then trailsHidePtr:=0;
    end;
end;

procedure ClearFood;
var fi:byte;
begin
     for fi:=0 to FOOD_MAX do f_y[fi] := NONE;
end;

procedure ShowPopulation;
var xoff:word;
begin
    NeoSetSolidFlag(1);
    NeoSetColor($f);
    tmpstr := 'Beings       ';
    NeoDrawString(244,2,tmpstr);
    NeoStr(swimmerCount,tmpstr);
    xoff := 318 - (Length(tmpstr) * 6);
    NeoDrawString(xoff,2,tmpstr);
end;

procedure ShowFood;
var xoff:word;
begin
    NeoSetSolidFlag(1);
    NeoSetColor($f);
    tmpstr := 'Food         ';
    NeoDrawString(244,12,tmpstr);
    NeoStr(foodCount,tmpstr);
    xoff := 318 - (Length(tmpstr) * 6);
    NeoDrawString(xoff,12,tmpstr);
end;

function GetFoodSlot:byte;
var fi:byte;
begin
    result := NONE;
    for fi:=0 to FOOD_MAX do begin
        if f_y[fi] = NONE then exit(fi);
    end;
end;

procedure AddFoodXY(x,y:word);
var fi:byte;
begin
    if foodCount > FOOD_MAX then exit;
    if x < POOL_LEFT then x := POOL_LEFT;
    if x > POOL_RIGHT then x := POOL_RIGHT;
    if y < POOL_TOP then y := POOL_TOP;
    if y > POOL_BOTTOM then y := POOL_BOTTOM;
    fi := GetFoodSlot;
    if fi <> NONE then begin
        f_x[fi] := x;
        f_y[fi] := y;
        NeoSetColor($a + NeoIntRandom(3));
        NeoDrawPixel(Trunc(f_x[fi]),Trunc(f_y[fi]));
        Inc(foodCount);
        ShowFood;
    end;
end;

procedure AddFood;
begin
    AddFoodXY(POOL_LEFT + NeoIntRandom(POOL_WIDTH+1),POOL_TOP + NeoIntRandom(POOL_HEIGHT+1));
end;

procedure SpreadFood(x,y:word;c:byte);
begin
    while c>0 do begin
        AddFoodXY(x-4+NeoIntRandom(9),y-4+NeoIntRandom(9));
        dec(c);
    end;
end;

procedure PushSwimmer(si:byte;force:byte;dir:word);
var x,y,tmp:float;

function GetVector(fn:byte):float;
begin
    SetMathVar(s_dir[si]);
    DoMathOnVar(fn);
    SetMathStack(m_float,0);
    SetMathStack(s_force[si],1);
    DoMathOnStack(MATHMul);
    result := GetMathStackFloat;
    SetMathVar(dir);
    DoMathOnVar(fn);
    SetMathStack(m_float,0);
    SetMathStack(force,1);
    DoMathOnStack(MATHMul);
    result := result + GetMathStackFloat;
end;

begin
    x := GetVector(MATHCos);
    y := GetVector(MATHSin);

    SetMathStack(x,0);
    SetMathStack(y,1);
    DoMathOnStack(9);
    s_dir[si] := Trunc(GetMathStackFloat);
    //Writeln(s_dir[si]);
    
    SetMathStack(x,0);
    SetMathStack(y,1);
    DoMathOnStack(MathDIST);
    s_force[si] := GetMathStackFloat;
    //Writeln(s_force[si]);
end;

function FindSlot:byte;
var si:byte;
begin
    result:=NONE;
    for si:=0 to SWIM_MAX do begin
        if s_energy[si]=0 then exit(si);
    end;
end;

procedure ClearSwimmers;
var si:byte;
begin
    for si := 0 to SWIM_MAX do begin
        s_energy[si] := 0;
        NeoHideSprite(si);
    end;
end;

function RandomIntRange(r_min,r_max:word):word;
var tmp:word;
begin
    if r_max = r_min then exit(r_min);
    if r_max < r_min then begin
        tmp := r_max;
        r_max := r_min;
        r_min := tmp;
    end;
    inc(r_max);
    result := r_min + NeoIntRandom(r_max - r_min); 
end;

procedure InitSwimmer(si:byte);
begin
    s_energy[si] := START_ENERGY;
    s_foodTarget[si] := NONE;
    s_mateTarget[si] := NONE;
    s_age[si] := 0;
    s_movet[si] := 0;
    s_force[si] := 0;
    s_dir[si] := 0;
    s_sprite[si] := 0;
    Inc(swimmerCount);
    ShowPopulation;    
end;

procedure AddSwimmerXY(x,y:word);
var si:byte;
begin
    if swimmerCount>SWIM_MAX then exit;
    si:=FindSlot;
    if si=NONE then exit;

    InitSwimmer(si);
    s_x[si] := x;
    s_y[si] := y;

    s_movefreq[si] := RandomIntRange(P_MOVE_FREQ_MIN,P_MOVE_FREQ_MAX);
    s_moveforce[si] := RandomIntRange(P_MOVE_FORCE_MIN,P_MOVE_FORCE_MAX);
    s_sighrange[si] := RandomIntRange(P_SIGHRANGE_MIN,P_SIGHRANGE_MAX);
    s_livelength[si] := 60 * RandomIntRange(P_LIVELEN_MIN,P_LIVELEN_MAX);
end;

procedure HatchSwimmer(si,mi:byte);
var ci,randomProperty:byte;
begin
    if swimmerCount>SWIM_MAX then exit;
    ci:=FindSlot;
    if ci=NONE then exit;

    InitSwimmer(ci);
    s_x[ci] := Trunc(s_x[si]);
    s_y[ci] := Trunc(s_y[si]);

    s_movefreq[ci] := RandomIntRange(s_movefreq[si],s_movefreq[mi]);
    s_moveforce[ci] := RandomIntRange(s_moveforce[si],s_moveforce[mi]);
    s_sighrange[ci] := RandomIntRange(s_sighrange[si],s_sighrange[mi]);
    s_livelength[ci] := 60 * RandomIntRange(s_livelength[si],s_livelength[mi]);

    randomProperty := NeoIntRandom(4);
    case randomProperty of
        0:  s_movefreq[si] := RandomIntRange(P_MOVE_FREQ_MIN,P_MOVE_FREQ_MAX);
        1:  s_moveforce[si] := RandomIntRange(P_MOVE_FORCE_MIN,P_MOVE_FORCE_MAX);
        2:  s_sighrange[si] := RandomIntRange(P_SIGHRANGE_MIN,P_SIGHRANGE_MAX);
        3:  s_livelength[si] := 60 * RandomIntRange(P_LIVELEN_MIN,P_LIVELEN_MAX);
    end;
end;

procedure AddSwimmer;
begin
    AddSwimmerXY(POOL_LEFT + NeoIntRandom(POOL_WIDTH+1),POOL_TOP + NeoIntRandom(POOL_HEIGHT+1));
end;

procedure SwimmerDies(si:byte);
begin
    s_energy[si]:=0;
    SpreadFood(Trunc(s_x[si]),Trunc(s_y[si]),5);
    NeoHideSprite(si);
    Dec(swimmerCount);
    ShowPopulation;
end;

function FindFood(si:byte):word;
var mindist,fi:byte;
    dx,dy:float;
    dist:word;
begin
    result := NeoIntRandom(360);
    s_foodTarget[si] := NONE;
    mindist := s_sighrange[si];
    for fi:=0 to FOOD_MAX do begin
        if f_y[fi] <> NONE then begin
            dx := f_x[fi] - s_x[si]; 
            dy := s_y[si] - f_y[fi];
            if (mindist>dx) and (mindist>dy) then begin
                SetMathStack(dx,0);
                SetMathStack(dy,1);
                DoMathOnStack(MathDIST);
                dist := Trunc(GetMathStackFloat);
                if dist <= mindist then begin
                    mindist := dist;
                    SetMathStack(dx,0);
                    SetMathStack(dy,1);
                    DoMathOnStack(9);
                    result := Trunc(GetMathStackFloat);
                    //Writeln(result);
                    s_foodTarget[si] := fi;
                end;
            end;
        end;
    end;
end;

function FindMate(si:byte):word;
var mindist,mi:byte;
    dx,dy:float;
    dist:word;
begin
    result := NeoIntRandom(360);
    s_mateTarget[si] := NONE;
    mindist := s_sighrange[si];
    for mi:=0 to SWIM_MAX do begin
        if (mi<>si) and (s_energy[mi] > T_MATING) then begin
            dx := (s_x[mi] - s_x[si]); 
            dy := (s_y[si] - s_y[mi]);
            SetMathStack(dx,0);
            SetMathStack(dy,1);
            DoMathOnStack(MathDIST);
            dist := Trunc(GetMathStackFloat);
            if dist <= mindist then begin
                mindist := dist;
                SetMathStack(dx,0);
                SetMathStack(dy,1);
                DoMathOnStack(9);
                result := Trunc(GetMathStackFloat);
                s_mateTarget[si] := mi;
            end;
        end;
    end;
end;

procedure ChooseTarget(si:byte);
var tforce:integer;
    tdir:word;
begin
    //s_x[si]:=Trunc(s_x[si]);
    //s_y[si]:=Trunc(s_y[si]);
    tforce := s_moveforce[si] + NeoIntRandom(3);
    s_energy[si] := s_energy[si] - tforce;
    if s_energy[si] <= 0 then SwimmerDies(si)
    else begin
        s_sprite[si]:=0;
        tdir := NeoIntRandom(360);
        if s_energy[si] < ENERGY_MAX then begin
            tdir := FindFood(si);
        end;
        if (s_energy[si] > T_MATING) and (swimmerCount <= SWIM_MAX) then begin
            tdir := FindMate(si);
            s_sprite[si]:=2;
        end;
        if s_energy[si] < T_HUNGRY then begin
            tforce := tforce + HUNGRY_HASTE;
            s_sprite[si]:=1;
        end;
        PushSwimmer(si,tforce,tdir);
    end;
end;

function Min(a,b:float):float;
begin
    if a < b then result := a else result := b;
end;

procedure DrawSwimmer(si:byte);
var x,y:word;
begin
    if s_energy[si]>0 then begin
        x := Trunc(s_x[si])-1;
        y := Trunc(s_y[si])-1;
        NeoUpdateSprite(si,x,y,s_sprite[si],0,7);
    end;
end;

procedure EatFood(si,fi:byte);
begin
    s_energy[si] := s_energy[si] + FOOD_ENERGY;
    s_foodTarget[si] := NONE;
    NeoSetColor(COLOR_WATER);
    NeoDrawPixel(Trunc(f_x[fi]),Trunc(f_y[fi]));
    Dec(foodCount);
    ShowFood;
    f_y[fi] := NONE;
end;

procedure Mate(si,mi:byte);
begin
    s_mateTarget[si] := NONE;
    s_mateTarget[mi] := NONE;
    s_energy[si] := T_HUNGRY;    
    s_energy[mi] := T_HUNGRY;    
    HatchSwimmer(si,mi);
end;

procedure Bounce(si:byte);
var a:shortInt;
begin
    a:=90;
    if NeoIntRandom(100)<50 then a := -a;
    s_dir[si] := (s_dir[si] + a) mod 360;
    s_force[si] := s_force[si] / 2;
end;

procedure MoveSwimmer(si:byte);
var speed,tmp,dx,dy:float;
    fi:byte;
begin
    
    if NeoIntRandom(100)>50 then AddTrail(si);
    
    SetMathStack(Min(30,s_force[si]),0);
    SetMathStack(float(MOVE_SPEED),1);
    DoMathOnStack(MATHMul);
    speed:=GetMathStackFloat;

    SetMathVar(s_dir[si]);
    DoMathOnVar(MATHCos);
    SetMathStack(m_float,0);
    SetMathStack(speed,1);
    DoMathOnStack(MATHMul);
    s_x[si] := s_x[si] + GetMathStackFloat;

    SetMathVar(s_dir[si]);
    DoMathOnVar(MATHSin);
    SetMathStack(m_float,0);
    SetMathStack(speed,1);
    DoMathOnStack(MATHMul);
    s_y[si] := s_y[si] - GetMathStackFloat;

    SetMathStack(s_force[si], 0);
    SetMathStack(float(FRICTION), 1);
    DoMathOnStack(MATHFDiv);
    s_force[si] := GetMathStackFloat;

    if s_x[si] < POOL_LEFT then begin s_x[si] := POOL_LEFT; Bounce(si); end;
    if s_x[si] > POOL_RIGHT then begin s_x[si] := POOL_RIGHT; Bounce(si); end;
    if s_y[si] < POOL_TOP then begin s_y[si] := POOL_TOP; Bounce(si); end;
    if s_y[si] > POOL_BOTTOM then begin s_y[si] := POOL_BOTTOM; Bounce(si); end;
    
    fi := s_foodTarget[si];
    if (fi <> NONE) and (f_y[fi]<>NONE) then begin
        dx := s_x[si] - f_x[fi];
        dy := s_y[si] - f_y[fi];
        SetMathStack(dx,0);
        SetMathStack(dy,1);
        DoMathOnStack(MathDIST);
        tmp := GetMathStackFloat;
        if tmp < 2 then EatFood(si, fi);
    end;

    fi := s_mateTarget[si];
    if (fi <> NONE) and (s_energy[fi]>0) then begin
        dx:=s_x[si] - s_x[fi];
        dy:=s_y[si] - s_y[fi];
        SetMathStack(dx,0);
        SetMathStack(dy,1);
        DoMathOnStack(MathDIST);
        tmp := GetMathStackFloat;
        if tmp < 2 then Mate(si, fi);
    end;
end;

procedure ReadPalettes;
var c,cc:byte;
    off:word;
    palettes: array [0..47] of byte;
begin
    repeat until not NeoBlitterBusy;
    NeoBlitterCopy(MEM_GFX+4,word(@palettes),48);
    off:=0;
    for c:=0 to 15 do begin
        NeoMessage.params[1] := palettes[off] shl 4;
        NeoMessage.params[2] := palettes[off+1] shl 4;
        NeoMessage.params[3] := palettes[off+2] shl 4;
        NeoMessage.params[0] := c;
        NeoSendMessage(5,32);
        NeoMessage.params[1] := palettes[off] and $f0;
        NeoMessage.params[2] := palettes[off+1] and $f0;
        NeoMessage.params[3] := palettes[off+2] and $f0;
        for cc:=0 to 15 do begin
            NeoMessage.params[0] := (c shl 4) + cc;
            NeoSendMessage(5,32);
        end;
        Inc(off,3);
    end;
end;

procedure ClearPond;
begin
    ClearSwimmers;
    ClearTrails;
    ClearFood;
    foodCount := 0;
    swimmerCount := 0;
    trailsShowPtr := 0;
    trailsHidePtr := 0;
    mButPrev := 0;
    autoFeeder := 0;
    NeoShowCursor(1);
    NeoSetSolidFlag(1);
    NeoSetColor(COLOR_WATER);
    NeoDrawRect(POOL_LEFT-1,POOL_TOP-1,POOL_RIGHT+1,POOL_BOTTOM+1);
    ShowPopulation;
    ShowFood;
    ShowFeeder;
    ShowNewBeing;
    ShowClear;
end;

begin

    SetDegreeMode;
    ReadPalettes;
    ClrScr;
    ClearPond;

    repeat
        NeoIntRandom(NONE);
        repeat until vblc<>NeoGetVblanks;
        vblc := NeoGetVblanks;

        count := 0;
        for i:=0 to SWIM_MAX do begin
            if s_energy[i]>0 then begin
                Inc(count);
                if s_age[i] > s_livelength[i] then begin
                    SwimmerDies(i);
                end else begin
                    if s_movet[i] = 0 then begin
                        s_movet[i] := s_movefreq[i];
                        ChooseTarget(i);
                    end else begin
                        if s_force[i] > 0.1 then begin  
                            MoveSwimmer(i);
                            DrawSwimmer(i);
                        end;
                    end;
                end;
                s_movet[i] := s_movet[i] - 1;
                s_age[i] := s_age[i] + 1;
            end;
        end;
        swimmerCount := count;

        if NeoIsMousePresent then begin
            NeoReadMouse;
            mBut := neoMouseButtons;
            mWheel := neoMouseWheel;
            mx := neoMouseX;
            my := neoMouseY;
            // feed
            if mBut and 1 <> 0 then 
                if manualFeedDelay = 0 then 
                    if (mx>POOL_LEFT) and (mx<POOL_RIGHT) and (my>POOL_TOP) and (my<POOL_BOTTOM) then AddFoodXY(mx,my);

            if ((mBut xor mButPrev) and mBut) = 2 then 
                if (mx>POOL_LEFT) and (mx<POOL_RIGHT) and (my>POOL_TOP) and (my<POOL_BOTTOM) then AddSwimmerXY(mx,my);


            if ((mBut xor mButPrev) and mBut) = 1 then begin
               
                if (mx>243) and (mx<318) and (my>BUT_FEEDER_TOP) and (my<BUT_FEEDER_TOP+23) then ToggleFeeder;
                if (mx>243) and (mx<318) and (my>BUT_NEW) and (my<BUT_NEW+23) then AddSwimmer;
                if (mx>243) and (mx<318) and (my>BUT_CLEAR) and (my<BUT_CLEAR+23) then ClearPond;

            end; 

            if KeyPressed then begin
                k:=ReadKey;
                case k of
                    'a','A':    ToggleFeeder;
                    'b','B':    AddSwimmer;
                    'p','P':    ClearPond;
                end;
            end;

        end;
        
        if manualFeedDelay > 0 then Dec(manualFeedDelay);

        if autoFeeder > 0 then begin
            Inc(autoFeederCounter);
            if autoFeederCounter>autoFeederDelay[autoFeeder] then begin
                autoFeederCounter := 0;
                AddFood;
            end;
        end;

        ExpireTrails;
        Inc(frame);
        mButPrev := mBut;
        mWheelPrev := mWheel;

    until false;

end.