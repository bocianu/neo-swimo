#!/usr/bin/bash
MPPATH=~/Mad-Pascal
MADSPATH=~/Mad-Assembler
NEOPATH=~/neo
NAME=swimo
read ORG < code.org

$MPPATH/mp ${NAME}.pas -target:neo -code:${ORG} && \
$MADSPATH/mads ${NAME}.a65 -x -i:${MPPATH}/base -o:${NAME}.bin && \
python ${NEOPATH}/exec.zip ${NAME}.bin@${ORG} ${NAME}.gfx@ffff run@${ORG} -o${NAME}.neo && \
$NEOPATH/neo ${NAME}.neo keys
